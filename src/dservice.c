/*
 DO WHATEVER YOU WANT TO PUBLIC LICENSE 
                    Version 1, May 2014 

 Copyright (C) 2014 pM0n <pmon.mail@gmail.com> 

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHATEVER YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHATEVER YOU WANT TO.
*/

#include <stdint.h>
#include <string.h>
#include "dservice.h"


// Types
typedef struct _LIST_ENTRY {
	struct _LIST_ENTRY  *Flink;
	struct _LIST_ENTRY  *Blink;
} LIST_ENTRY;

typedef struct _PEB_LDR_DATA { 
	uint32_t                Length;
	uint8_t                 Initialized;
	void*                   SsHandle;
	LIST_ENTRY              InLoadOrderModuleList;
	LIST_ENTRY              InMemoryOrderModuleList;
	LIST_ENTRY              InInitializationOrderModuleList;
#if 0
	uint8_t			Uknown1[8];
	void*			Uknown2[3];
	LIST_ENTRY		InMemoryOrderModuleList;
#endif
} PEB_LDR_DATA;

typedef struct _LDR_DATA_TABLE_ENTRY {
	void* Reserved1[2];
	LIST_ENTRY InMemoryOrderLinks;
	void* Reserved2[2];
	void* DllBase;
	void* EntryPoint;
	void* Reserved3;
	void* FullDllName;
	uint8_t Reserved4[8];
	void* Reserved5[3];
	union {
		uint32_t CheckSum;
		void* Reserved6;
	};
	uint32_t TimeDateStamp;
} LDR_DATA_TABLE_ENTRY;

typedef struct _PEB {
	uint8_t                         Uknown1[2];
	uint8_t                         BeingDebugged;
	uint8_t                         Uknown2[1];
	void							*Uknown3[2];
	PEB_LDR_DATA					*Ldr;
	void							*ProcessParameters;
	uint8_t                         Uknown4[104];
	void*							Uknown5[52];
	void							*PostProcessInitRoutine;
	uint8_t                         Uknown6[128];
	void							*Uknown7[1];
	uint32_t                        SessionId;
} PEB;

typedef struct _IMAGE_DATA_DIRECTORY {
    uint32_t   VirtualAddress;
    uint32_t   Size;
} IMAGE_DATA_DIRECTORY;

typedef struct _IMAGE_EXPORT_DIRECTORY {
    uint32_t   Characteristics;
    uint32_t   TimeDateStamp;
    uint16_t    MajorVersion;
    uint16_t    MinorVersion;
    uint32_t   Name;
    uint32_t   Base;
    uint32_t   NumberOfFunctions;
    uint32_t   NumberOfNames;
    uint32_t   AddressOfFunctions;     
    uint32_t   AddressOfNames;
    uint32_t   AddressOfNameOrdinals;
} IMAGE_EXPORT_DIRECTORY, *PIMAGE_EXPORT_DIRECTORY;

typedef struct _IMAGE_OPTIONAL_HEADER {
    uint16_t    Magic;
    uint8_t    MajorLinkerVersion;
    uint8_t    MinorLinkerVersion;
    int32_t   SizeOfCode;
    int32_t   SizeOfInitializedData;
    int32_t   SizeOfUninitializedData;
    int32_t   AddressOfEntryPoint;
    int32_t   BaseOfCode;
    int32_t   BaseOfData;
    int32_t   ImageBase;
    int32_t   SectionAlignment;
    int32_t   FileAlignment;
    uint16_t    MajorOperatingSystemVersion;
    uint16_t    MinorOperatingSystemVersion;
    uint16_t    MajorImageVersion;
    uint16_t    MinorImageVersion;
    uint16_t    MajorSubsystemVersion;
    uint16_t    MinorSubsystemVersion;
    int32_t   Win32VersionValue;
    int32_t   SizeOfImage;
    int32_t   SizeOfHeaders;
    int32_t   CheckSum;
    uint16_t    Subsystem;
    uint16_t    DllCharacteristics;
    int32_t   SizeOfStackReserve;
    int32_t   SizeOfStackCommit;
    int32_t   SizeOfHeapReserve;
    int32_t   SizeOfHeapCommit;
    int32_t   LoaderFlags;
    int32_t   NumberOfRvaAndSizes;
    IMAGE_DATA_DIRECTORY DataDirectory[0x10];
} IMAGE_OPTIONAL_HEADER;

typedef void*  (__stdcall *getprocaddress_t)(DLL_HANDLE module, const char *name);
typedef DLL_HANDLE (__stdcall *load_library_ex_t)(const char* file_name, uint32_t handle, uint32_t flags);
typedef	DLL_HANDLE (__stdcall *get_module_handle_t)(const char* file_name);

// Function declarations
void* GetPebH();
static void* FindKernel32Base();
static void* PrivateGetProcAddr(const void* module,const char *FunctionName);

// Variables
static getprocaddress_t getprocaddress = 0;
static load_library_ex_t load_library_ex = 0;
static get_module_handle_t get_module_handle = 0;

/******************************************************************************
* Public functions
******************************************************************************/

// Initialize the pointers
uint8_t DllServiceInit()
{
	DLL_HANDLE kernelbase;
	
	kernelbase = FindKernel32Base();
	if (!kernelbase)
	{
		// Bad
		return -1;
	}

	getprocaddress = (getprocaddress_t)PrivateGetProcAddr(kernelbase,"GetProcAddress");
	load_library_ex = (load_library_ex_t)PrivateGetProcAddr(kernelbase,"LoadLibraryExA");
	get_module_handle = (get_module_handle_t)PrivateGetProcAddr(kernelbase,"GetModuleHandleA");

	if (!getprocaddress || !load_library_ex || !get_module_handle)
	{
		// Another bad (problem with the names)
		return -1;
	}

	return 0;

}

void* DllServiceGetProcAddress(DLL_HANDLE module, const char* function_name)
{
	if (getprocaddress)
	{
		return getprocaddress(module,function_name);
	}
	return 0;
}

DLL_HANDLE DllServiceLoadLibraryEx(const char* file_name, uint32_t file_handle, uint32_t flags)
{
	if (load_library_ex)
	{
		return load_library_ex(file_name,file_handle,flags);
	}
	return 0;
}

DLL_HANDLE DllServiceGetModuleHandle(const char* file_name)
{
	if (get_module_handle)
	{
		return get_module_handle(file_name);
	}
	return 0;
}

/******************************************************************************
* Private functions
******************************************************************************/

// Function returning a pointer to the PEB
//
__declspec(naked) void* GetPebH()
{

// Don't care that the function is not returning a value
#pragma warning(default:4716)

#ifdef _M_ARM
	// WoA platforms
	__emit(0xEE1D); // MRC p15, 0, R0,c13,c0, 2
	__emit(0x0F50); // MRC p15, 0, R0,c13,c0, 2
	__emit(0x6b00);     // LDR R0, [R0,#0x30]
	__emit(0x46F7);		// MOV PC, LR

#else
	// x86 
	__asm {
		mov    eax, fs:0x30
		ret
	};
#endif
	
}

// Function returning the base address of kernel32.dll
//
static void* FindKernel32Base() {
	PEB *peb; 
	unsigned char *pebH,*ldr,*modlist,*flink,*dllbase;

	pebH = (unsigned char*)GetPebH();
	peb = (PEB *)pebH;
	ldr = (unsigned char*) peb->Ldr;
	modlist = *((unsigned char **)(ldr+0x1C));
	flink = *((unsigned char **)(modlist));
	dllbase = *((unsigned char **)(flink+0x8));

	if (dllbase[0] == 'M' && dllbase[1] == 'Z')
		return dllbase;
	return 0;
}

// Function that works like GetProcAddress
//
static void* PrivateGetProcAddr(const void* module,const char *FunctionName)
{
	unsigned char *ptr = (unsigned char*)module;
	unsigned char *pe_header;
	IMAGE_OPTIONAL_HEADER *optional_header;
	IMAGE_EXPORT_DIRECTORY *eta;
	uint32_t i;
	
	if (!(ptr[0] == 'M' && ptr[1] == 'Z')) return 0;

	//pe_header = *((unsigned char **)(ptr+0x3c));
	pe_header = (ptr+0xf0);

	if (!(pe_header[0] == 'P' && pe_header[1] == 'E')) return 0;

	optional_header = (IMAGE_OPTIONAL_HEADER *)(pe_header + 24);

	eta = (IMAGE_EXPORT_DIRECTORY *)((unsigned char*)module + optional_header->DataDirectory[0].VirtualAddress);

	for (i = 0; i < eta->NumberOfNames; i++)
	{
		int32_t rva_of_name =((int32_t*)((int32_t)module + eta->AddressOfNames))[i];
		const char *iname = (const char*)module + rva_of_name ;
		//OutputDebugStringA(iname);
		//OutputDebugStringA("\n");
		if (strcmp(FunctionName,iname) == 0) {
			uint16_t ordinal_index =((uint16_t*)((int32_t)module + eta->AddressOfNameOrdinals))[i];
			void *fptr = (void*)((unsigned char*)module + (int32_t)((void**)((int32_t)module + eta->AddressOfFunctions))[ordinal_index]);
			return fptr;
		}

	}
	
	return 0;



}