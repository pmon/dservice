/*
 DO WHATEVER YOU WANT TO PUBLIC LICENSE 
                    Version 1, May 2014 

 Copyright (C) 2014 pM0n <pmon.mail@gmail.com> 

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHATEVER YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHATEVER YOU WANT TO.
*/
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void* DLL_HANDLE;

/* 
	Function to init this mini library

	Parses Windows internal structures to find the information
	needed for this lib. It should be called at least once before
	using the features of the lib. Multiple calls useless.

	Returns 0 on success
*/
uint8_t DllServiceInit();

/*
	Works like Win32 GetProcAddress
*/
void* DllServiceGetProcAddress(DLL_HANDLE module, const char* function_name);

/*
	Works like Win32 LoadLibraryEx
*/
DLL_HANDLE DllServiceLoadLibraryEx(const char* file_name, uint32_t file_handle, uint32_t flags);

/*
	Works like Win32 GetModuleHandle
*/
DLL_HANDLE DllServiceGetModuleHandle(const char* file_name);

#ifdef __cplusplus
}
#endif